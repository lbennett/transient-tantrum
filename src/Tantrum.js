const Client = require('./Client');
const setTimeoutPromise = require('./setTimeoutPromise');

const Tantrum = {
  client: undefined,
  projectPath: undefined,
  mergeRequestId: undefined,
  jobName: undefined,
  retryCount: undefined,
  pipeline: undefined,
  waitDuration: undefined,

  init() {
    this.client = Client.create(process.env.GITLAB_PRIVATE_ACCESS_TOKEN);

    return this;
  },

  run({
    projectPath,
    mergeRequestId,
    jobName,
    retryCount,
    waitDuration,
    invert,
  }) {
    this.projectPath = projectPath;
    this.mergeRequestId = mergeRequestId;
    this.jobName = jobName;
    this.retryCount = retryCount;
    this.waitDuration = waitDuration;
    this.invert = invert;

    console.log('Starting tantrum...');
    if (this.invert) console.log('Invert mode enabled.');

    return this.client
      .getLatestMergeRequestPipeline(projectPath, mergeRequestId)
      .then(pipeline => (this.pipeline = pipeline))
      .then(pipeline =>
        this.client.getLatestJobByName(projectPath, pipeline.id, jobName),
      )
      .then(job => this.report(job));
  },

  checkJob(job) {
    console.log(`Checking pipeline in ${this.waitDuration} minutes...`);

    return setTimeoutPromise(60 * 1000 * this.waitDuration)
      .then(() => this.client.getJob(this.projectPath, job.id))
      .then(job => this.report(job));
  },

  report(job) {
    console.log(this.statusMessage(job));

    if (job.status === 'running') return this.checkJob(job);

    return (job.status === 'success' && !this.invert) ||
      (job.status !== 'success' && this.invert)
      ? this.retryJob(job)
      : this.complete(job);
  },

  complete(job) {
    console.log(`Tantrum complete. ${this.statusMessage(job)}`);
  },

  statusMessage(job) {
    return `The \`${job.name}\` jobs status is currently "${job.status}".`;
  },

  retryJob(job) {
    if (this.retryCount === 0) return this.reportSuccess();

    return this.client.retryJob(this.projectPath, job.id).then(job => {
      this.retryCount -= 1;
      return this.checkJob(job);
    });
  },
};

module.exports = Tantrum;
