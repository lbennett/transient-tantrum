const Yargs = require('yargs');
const Tantrum = require('./Tantrum');

const Cli = {
  tantrum: undefined,
  GITLAB_PRIVATE_ACCESS_TOKEN: undefined,

  init(GITLAB_PRIVATE_ACCESS_TOKEN) {
    this.tantrum = Tantrum.init(GITLAB_PRIVATE_ACCESS_TOKEN);

    this.configureYargs().argv;
  },

  setupPositionalYargs(yargs) {
    return yargs
      .positional('projectPath', {
        describe:
          'Path to the project containing the MR to test e.g. gitlab-org/gitlab-ce.',
        string: true,
      })
      .positional('mergeRequestId', {
        describe: 'ID of the Merge request to test e.g. 1234.',
        number: true,
      })
      .positional('jobName', {
        describe: 'Name of the job to retry, e.g. karma, or "docs lint".',
        string: true,
      })
      .positional('retryCount', {
        describe: 'Number of times to retry the job if it succeeds',
        default: 5,
        number: true,
      })
      .option('waitDuration', {
        describe:
          'The duration in minutes to wait between checking the job status',
        alias: 'w',
        default: 3,
        number: true,
      })
      .option('invert', {
        describe: 'Retry on failure instead of success',
        alias: 'i',
        default: false,
        boolean: true,
      });
  },

  configureYargs() {
    return Yargs.command(
      '* <projectPath> <mergeRequestId> <jobName> [retryCount] [-wivh]',
      'start a transient tantrum on a specific job for a given merge request',
      yargs => this.setupPositionalYargs(yargs),
      argv => this.tantrum.run(argv),
    ).alias('h', 'help');
  },
};

module.exports = Cli;
